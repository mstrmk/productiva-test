<?php

require_once __DIR__.'/App.php';

$app = new App();

if (isset($_GET['ticker'])) {
  $answer = $app->apiCall('search', ['ticker' => $_GET['ticker']]);

  if ($answer->status == 'error') {
    echo 'Ошибка<br>'.$answer->data;
  }
  else {
    $item = $answer->data;
    foreach ($item as $propName => $prop) {
      echo '<p>'.$propName.': '.$prop.'</p>';
    }
  }

  echo '<p><a href = "'.$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].explode('?', $_SERVER['REQUEST_URI'])[0].'">Назад к списку</a></p>';
}
else {
  $answer = $app->apiCall('get_all');

  if ($answer->status == 'error') {
    echo 'Ошибка<br>'.$answer->data;
  }
  else {
    $list = $answer->data;
    // echo '<pre>';
    // print_r($list);
    // echo '</pre>';
    $currentUrl = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    foreach ($list as $item) {
      echo '<a href = "'.$currentUrl.'?ticker='.$item->ticker.'"><button>'.$item->ticker.'</button>';
    }
  }
}

?>
