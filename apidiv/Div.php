<?php

class Div
{

  private $list = null;

	public function get() {
		$data = file_get_contents(__DIR__.'/div.csv');
		$data = explode("\r\n", $data);
		return $data;
	}

  public function parse() {
    if (is_null($this->list)) {
      $resultList = [];
      $propNames = [
        'ticker',
        'date_pay',
        'amount',
        'date_ex',
      ];

      $list = $this->get();

      foreach ($list as $key => $item) {
        $item = trim($item);
        if ($item != '') {
          $item = explode(';', trim($item));
          $newItem = [];
          foreach ($item as $keyProp => $prop) {
            $propName = isset($propNames[$keyProp]) ? $propNames[$keyProp] : $keyProp;
            $newItem[$propName] = $prop;
          }
          $resultList[$newItem['ticker']] = $newItem;
        }
      }
      $this->list = $resultList;
    }

    return $this->list;
  }

  public function getList() {
    return $this->list;
  }

}

?>
