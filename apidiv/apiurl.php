<?php

require_once __DIR__.'/Div.php';
require_once __DIR__.'/Api.php';

$params = $_GET;

$method = isset($params['method']) ? $params['method'] : null;
unset($params['method']);

$token = isset($params['token']) ? $params['token'] : null;
unset($params['token']);

$api = new Api();
$api->setToken($token);

echo $api->call($method, $params);

?>
