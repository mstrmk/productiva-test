<?php

class Api
{

  const TOKEN = 'tk';

  private $div = null;
  private $validToken = null;
  private $token = null;

  private function checkToken() {
    if (is_null($this->validToken)) {
      $this->validToken = $this->token == self::TOKEN;
    }
    return $this->validToken;
  }

  public function setToken($token) {
    $this->token = $token;
  }

  private function response($data = []) {
    return json_encode($data);
  }

  private function responseSuccess($data = 'success') {
    return $this->response([
      'status' => 'success',
      'data' => $data
    ]);
  }

  private function responseError($data = 'error') {
    return $this->response([
      'status' => 'error',
      'data' => $data
    ]);
  }

  private function getDiv() {
    if (is_null($this->div)) {
      $this->div = new Div();
    }
    return $this->div;
  }

  private function method_getAll() {
    $div = $this->getDiv();
    $div->parse();
    return $div->getList();
  }

  private function method_search($ticker = null) {
    if ($ticker) {
      $div = $this->getDiv();
      $div->parse();
      $list = $div->getList();

      if (isset($list[$ticker])) {
        return $list[$ticker];
      }
    }

    return false;
  }

  public function call($method = null, $params = []) {
    if (!$this->checkToken()) {
      return $this->responseError('invalid token');
    }

    if ($method) {
      switch ($method) {
        case 'get_all':
          $list = $this->method_getAll();
          if ($list !== false) {
            return $this->responseSuccess($list);
          }
          else {
            return $this->responseError();
          }
        break;
        case 'search':
          if (!isset($params['ticker'])) {
            return $this->responseError('ticker not passed');
          }

          $item = $this->method_search($params['ticker']);
          if ($item !== false) {
            return $this->responseSuccess($item);
          }
          else {
            return $this->responseError('ticker not found');
          }
        break;
        default:
          return $this->responseError('method not found');
        break;
      }
    }
    else {
      return $this->responseError('method not passed');
    }
  }

}

?>
