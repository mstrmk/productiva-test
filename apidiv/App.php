<?php

class App
{

  public function apiCall($method, $params = []) {
    // url до API
    $apiUrl = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].str_replace('index.php', '', explode('?', $_SERVER['REQUEST_URI'])[0]).'apiurl.php';

    $params = array_merge([
      'token' => 'tk',
      'method' => $method,
    ], $params);

    $paramsString = http_build_query($params);

    $ch = curl_init($apiUrl.'?'.$paramsString);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $result = curl_exec($ch);
    $result = json_decode($result);

    return $result;
  }

}

?>
